import docker
import os
from abc import ABC, abstractmethod
from docker.models.containers import Container
from testinfra.utils.ansible_runner import AnsibleRunner
from typing import Dict, List


class MoleculeHost(ABC):
    def __init__(self, name: str, ansible_runner: AnsibleRunner):
        self._name = name
        self._ansible_runner = ansible_runner
        self._facts = None

    @property
    def name(self) -> str:
        return self._name

    @property
    @abstractmethod
    def ip(self) -> str:
        pass

    @property
    def facts(self) -> Dict:
        if self._facts is None:
            # WARNING: Facts gathering is slow! Activate only if really needed!
            self._facts = self._ansible_runner.run_module(self.name, 'setup', None)['ansible_facts']
        return self._facts


class DockerMoleculeHost(MoleculeHost):
    def __init__(self, name: str, ansible_runner: AnsibleRunner, container: Container):
        super().__init__(name, ansible_runner)
        self._container = container
        self._ip = container.attrs['NetworkSettings']['IPAddress']

    @property
    def ip(self) -> str:
        return self._ip


class VagrantMoleculeHost(MoleculeHost):
    def __init__(self, name: str, ansible_runner: AnsibleRunner):
        super().__init__(name, ansible_runner)
        self._ip = None

    @property
    def ip(self) -> str:
        if self._ip is None:
            self._ip = self.facts['ansible_eth1']['ipv4']['address']
        return self._ip


class MoleculeHosts:
    def __init__(self):
        self._ansible_runner = AnsibleRunner(os.environ['MOLECULE_INVENTORY_FILE'])
        self._docker_hosts = {}
        self._vagrant_hosts = {}

    @property
    def ansible_runner(self) -> AnsibleRunner:
        return self._ansible_runner

    def docker_hosts_in(self, group_name: str) -> List[MoleculeHost]:
        return self._docker_hosts_in(group_name).values()

    def docker_host_names_in(self, group_name: str) -> List[str]:
        return self._docker_hosts_in(group_name).keys()

    def _docker_hosts_in(self, group_name: str) -> Dict[str, MoleculeHost]:
        if group_name not in self._docker_hosts:
            docker_client = docker.from_env()
            host_names = self.ansible_runner.get_hosts(group_name)
            self._docker_hosts[group_name] = {
                host_name: DockerMoleculeHost(host_name, self._ansible_runner, docker_client.containers.get(host_name))
                for host_name in host_names
            }
        return self._docker_hosts[group_name]

    def vagrant_hosts_in(self, group_name: str) -> List[MoleculeHost]:
        return self._vagrant_hosts_in(group_name).values()

    def vagrant_host_names_in(self, group_name: str) -> List[str]:
        return self._vagrant_hosts_in(group_name).keys()

    def _vagrant_hosts_in(self, group_name: str) -> Dict[str, MoleculeHost]:
        if group_name not in self._vagrant_hosts:
            host_names = self.ansible_runner.get_hosts(group_name)
            self._docker_hosts[group_name] = {
                host_name: VagrantMoleculeHost(host_name, self._ansible_runner)
                for host_name in host_names
            }
        return self._docker_hosts[group_name]
