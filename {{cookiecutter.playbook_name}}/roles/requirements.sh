#! /bin/bash

set -e

SCRIPT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

function main() {
    printf "Installing required roles with Ansible Galaxy\\n"
    ansible-galaxy install \
        --role-file "$SCRIPT_DIR/requirements.yml" \
        --roles-path "$SCRIPT_DIR" \
        --force
}

main "$@"
