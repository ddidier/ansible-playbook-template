
# Ansible Playbook Template

<!-- MarkdownTOC -->

1. [Installation](#installation)
1. [Usage](#usage)
    1. [Choose the test driver](#choose-the-test-driver)
    1. [Create a new playbook](#create-a-new-playbook)
    1. [Test the new playbook](#test-the-new-playbook)
1. [Development](#development)
    1. [Manage dependencies](#manage-dependencies)
    1. [Test the template](#test-the-template)
1. [References](#references)

<!-- /MarkdownTOC -->



A template for an Ansible playbook including the testing framework.

The testing framework is [Molecule].

The templating engine is [CookieCutter].



<a id="installation"></a>
## Installation

The Python package manager is [PIP].

We strongly suggest that you use [pyenv].

Testing requires [Docker] and/or [Vagrant] with [VirtualBox] 6.0.

On Ubuntu 18.04:

```bash
# install some useful dependencies
sudo apt install -y build-essential libssl-dev libffi-dev python-dev

# install PIP and VirtualEnv
sudo apt install -y python3-pip python3-venv

# install pyenv, and
# do not forget to read the installation output!
curl https://pyenv.run | bash

# clone the template repository
git clone https://gitlab.com/ddidier/ansible-playbook-template.git
cd ansible-playbook-template

# create and activate the environments as given in the '.python-version' file
pyenv install 3.8.6
pyenv virtualenv 3.8.6 ansible-playbook-template_3.8.6
pyenv activate ansible-playbook-template_3.8.6
pip install --upgrade pip

# install all the required dependencies
pip install --requirement requirements.txt
```



<a id="usage"></a>
## Usage

Before creating a new playbook, you must choose which test driver to use.

<a id="choose-the-test-driver"></a>
### Choose the test driver

The new role can be tested with [Docker] or [Vagrant] (or both).

If you want to use Docker as the test driver, be aware that:

- the file `/etc/hosts` is managed by Docker and therefore cannot be modified
- the file `/etc/resolv.conf` is managed by Docker and therefore cannot be modified

If you want to use Vagrant as the test driver, be aware that:

- the driver is [advertised as being in alpha stage](https://molecule.readthedocs.io/en/stable/configuration.html#vagrant).

<a id="create-a-new-playbook"></a>
### Create a new playbook

Use the script `bin/new-playbook` to create a new playbook.

You can pass some of (or all) the required parameters on the command line.
You will be prompted for the remaining ones.

```bash
% ./bin/new-playbook --help
Usage: new-playbook [OPTIONS]

  Create an Ansible playbook from a template including a testing framework.

Options:
  --playbook-name TEXT            The playbook name, e.g. ndd-web-servers
  --playbook-namespace TEXT       The playbook namespace, e.g. ndd_web_servers
  --playbook-description TEXT     The playbook description
  --playbook-directory DIRECTORY  The playbook directory
  --driver-name [docker|vagrant]  The test driver
  --info                          Set the debug level to INFO
  --debug                         Set the debug level to DEBUG
  --help                          Show this message and exit.
```

Here's an example with all the required parameters:

```bash
./bin/new-playbook --playbook-directory=/tmp \
                   --playbook-name=ndd_web_servers \
                   --playbook-namespace=ndd_web_servers \
                   --playbook-description="A supercharged web service" \
                   --driver-name=vagrant
```

The template is also an example installing Apache [HTTPd]:
remove the parts that you don't need from the generated project!

<a id="test-the-new-playbook"></a>
### Test the new playbook

Instructions are given in the `README` of the generated playbook.



<a id="development"></a>
## Development

This section is about the development of the template, not the generated playbook.

<a id="manage-dependencies"></a>
### Manage dependencies

Don't forget to update the Python requirements as needed in:

- `{{cookiecutter.playbook_name}}/molecule/requirements.txt` for the generated project
- `requirements.txt` for the project generator itself

<a id="test-the-template"></a>
### Test the template

Don't forget to install [Docker] and/or [Vagrant] before running the tests!

Test the project with the script `bin/test`:

```bash
% ./tests/test --help
Usage: test <OPTIONS...>
   --all      Test all drivers
   --docker   Test Docker driver
   --vagrant  Test Vagrant driver
OPTIONS:
   --debug                 Increase the output verbosity
   --keep-generated-files  Do not delete the generated project files
```

The script returns `0` on success, `1` on failure.



<a id="references"></a>
## References

[Ansible]: https://www.ansible.com/
[CookieCutter]: https://github.com/audreyr/cookiecutter
[Docker]: https://www.docker.com/
[HTTPd]: https://httpd.apache.org/
[Molecule]: https://molecule.readthedocs.io/
[PIP]: https://en.wikipedia.org/wiki/Pip_(package_manager)
[pyenv]: https://github.com/pyenv/pyenv
[pytest]: https://docs.pytest.org/
[testinfra]: https://testinfra.readthedocs.io/
[Vagrant]: https://www.vagrantup.com/
[VirtualBox]: https://www.virtualbox.org/
[VirtualEnv]: https://virtualenv.pypa.io/

- [Ansible]
- [CookieCutter]
- [Docker]
- [HTTPd]
- [Molecule]
- [PIP]
- [pyenv]
- [pytest]
- [testinfra]
- [Vagrant]
- [VirtualBox]
- [VirtualEnv]
