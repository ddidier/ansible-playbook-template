#! /usr/bin/env python3

import logging
import os
import re
import sys
import time
from datetime import datetime
from pathlib import Path
from typing import Dict
from typing import List

import click
import git
import pkg_resources
from cookiecutter.main import cookiecutter


# -------------------------------------------------------------------------------------------------- Git Utilities -----

# TODO: extract into a reusable library

class GitRepository:
    """
    Utilities dealing with ``git.Repository``.
    """

    def __init__(self, repository: git.Repo):
        self.repository = repository

    def head_commit(self) -> git.Commit:
        return self.repository.head.commit

    def head_tags(self) -> List[git.Tag]:
        head_commit = self.head_commit()
        return list(tag for tag in self.repository.tags if tag.commit == head_commit)

    def head_tag_names(self) -> List[str]:
        return [tag.name for tag in self.head_tags()]

    def head_metadata(self) -> Dict:
        return {
            'commit_hash': self.head_commit().hexsha,
            'commit_date': self.head_commit().committed_datetime,
            'commit_tags': self.head_tag_names(),
            'generated_at': datetime.now(),
            'modified': self.is_modified(),
        }

    def is_index_different_from_head(self) -> bool:
        """
        :return: True if the index and the commit’s tree your HEAD points to are different, False otherwise
        """
        return len(self.repository.index.diff(self.head_commit())) > 0

    def is_index_different_from_working_tree(self) -> bool:
        """
        :return: True if the index and the working tree are different, False otherwise
        """
        return len(self.repository.index.diff(None)) > 0

    def has_untracked_files(self) -> bool:
        """
        :return: True if there are untracked files, False otherwise
        """
        return len(self.repository.untracked_files) > 0

    def is_modified(self) -> bool:
        return self.is_index_different_from_head() \
               or self.is_index_different_from_working_tree() \
               or self.has_untracked_files()

    def commits_behind(self) -> List[git.Commit]:
        active_branch_name = self.repository.active_branch.name
        return list(self.repository.iter_commits(f'{active_branch_name}..{active_branch_name}@{{u}}'))

    def commits_ahead(self) -> List[git.Commit]:
        active_branch_name = self.repository.active_branch.name
        return list(self.repository.iter_commits(f'{active_branch_name}@{{u}}..{active_branch_name}'))

    @staticmethod
    def initialize(directory: Path) -> 'GitRepository':
        repository = git.Repo.init(directory)
        repository.index.commit("Initial commit")
        return repository


class GitTemplate:

    def __init__(self, git_repository: GitRepository):
        self._git_repository = git_repository

    def interpolate(self, file_path: Path, key_prefix: str = ''):
        git_metadata = {
            'generated_at': datetime.now(),
            'modified': None,
            'commit_hash': None,
            'commit_date': None,
            'commit_tags': None,
        }

        if self._git_repository is not None:
            git_metadata = self._git_repository.head_metadata()

        printable_git_metadata = {k: '' if v is None else v for k, v in git_metadata.items()}
        printable_git_metadata = {
            'generated_at': self._datetime_to_string(printable_git_metadata['generated_at']),
            'modified': str(printable_git_metadata['modified']),
            'commit_hash': printable_git_metadata['commit_hash'],
            'commit_date': self._datetime_to_string(printable_git_metadata['commit_date']),
            'commit_tags': ', '.join(printable_git_metadata['commit_tags']),
        }

        generated_readme_content = file_path.read_text()
        generated_readme_content = generated_readme_content \
            .replace(f'{key_prefix}GENERATION_DATE', printable_git_metadata['generated_at']) \
            .replace(f'{key_prefix}MODIFIED', printable_git_metadata['modified']) \
            .replace(f'{key_prefix}COMMIT_HASH', printable_git_metadata['commit_hash']) \
            .replace(f'{key_prefix}COMMIT_DATE', printable_git_metadata['commit_date']) \
            .replace(f'{key_prefix}COMMIT_TAGS', printable_git_metadata['commit_tags'])
        file_path.write_text(generated_readme_content)

    @staticmethod
    def _datetime_to_string(dt) -> str:
        if dt:
            return time.strftime("%Y/%m/%d %H:%M:%S", dt.timetuple())
        else:
            return 'N/A'


# ----------------------------------------------------------------------------------------------------------------------


def script_directory() -> Path:
    return Path(sys.argv[0]).resolve().parent


def template_directory() -> Path:
    return script_directory().parent


def check_requirements():
    logging.debug('Checking requirements')

    missing_requirements = []

    with template_directory().joinpath('requirements.txt').open() as requirements_file:
        for requirement in requirements_file:
            requirement = requirement.strip()
            try:
                logging.debug("Checking requirement '%s'", requirement)
                pkg_resources.require(requirement)
            except pkg_resources.DistributionNotFound:
                missing_requirements.append(requirement)

    if len(missing_requirements) > 0:
        missing_requirements_string = "\n  ".join(missing_requirements)
        message = f'FATAL: This script requires the following missing packages\n  {missing_requirements_string}'
        sys.exit(click.style(message, fg='red'))


# ------------------------------------------------------------------------------------------------------------ CLI -----

CONTEXT_SETTINGS = dict(help_option_names=['-h', '--help'])


# @click.pass_context
# def default_playbook_namespace(context):
#     playbook_name = context.params['playbook_name']
#     return re.sub(r'[\s-]', '_', playbook_name)


def validate_playbook_name(context, parameter, value):
    if re.match(r"(^[a-z0-9_]+$)", value):
        return value

    click.echo('Playbook names are limited to lowercase word characters and "_" i.e. [a-z0-9_]')
    value = click.prompt(parameter.prompt)
    return validate_playbook_name(context, parameter, value)


def validate_playbook_namespace(context, parameter, value):
    if re.match(r"(^[a-z0-9_]+$)", value):
        return value

    click.echo('Playbook namespaces are limited to lowercase word characters and "_" i.e. [a-z0-9_]')
    value = click.prompt(parameter.prompt)
    return validate_playbook_namespace(context, parameter, value)


@click.command()
@click.option('--playbook-name', type=str, callback=validate_playbook_name,
              prompt='The playbook name', help='The playbook name, e.g. ndd-web-server')
@click.option('--playbook-namespace', type=str, callback=validate_playbook_namespace,
              prompt='The playbook namespace', help='The playbook namespace, e.g. ndd_web_server')
@click.option('--playbook-description', type=str, default='"TODO: Add a meaningful description"',
              prompt='The playbook description', help='The playbook description')
@click.option('--playbook-directory', type=click.Path(exists=True, file_okay=False, dir_okay=True), default=os.getcwd(),
              prompt='The playbook directory', help='The playbook directory')
@click.option('--driver-name', type=click.Choice(['docker', 'vagrant']), default='docker',
              prompt='The test driver ("docker" or "vagrant")', help='The test driver')
@click.option('--info', 'logging_level',
              flag_value=20,
              help='Set the debug level to INFO')
@click.option('--debug', 'logging_level',
              flag_value=10,
              help='Set the debug level to DEBUG')
def new_playbook(playbook_name, playbook_namespace, playbook_description, playbook_directory, driver_name,
                 logging_level=30):
    """
    Create an Ansible playbook from a template including a testing framework.

    You can pass some of (or all) the required parameters on the command line.
    You will be prompted for the remaining ones.
    """

    logging.basicConfig(
        level=logging_level,
        stream=sys.stdout,
        format='%(asctime)s | %(levelname)8s | %(name)-25.25s | %(message)s',
        datefmt="%Y-%m-%d %H:%M:%S")

    logging.debug('playbook_name = %s', playbook_name)
    logging.debug('playbook_namespace = %s', playbook_namespace)
    logging.debug('playbook_description = %s', playbook_description)
    logging.debug('playbook_directory = %s', playbook_directory)
    logging.debug('driver_name = %s', driver_name)

    # Generate Ansible project
    molecule_context = {
        'playbook_name': playbook_name,
        'playbook_namespace': playbook_namespace,
        'playbook_description': playbook_description,
        'playbook_directory': playbook_directory,
        'driver_name': driver_name,
    }

    cookiecutter(template=template_directory().as_posix(),
                 output_dir=playbook_directory,
                 extra_context=molecule_context,
                 no_input=True)

    # Replace GIT data placeholders in the README file
    template_git_directory = template_directory()
    template_git_repository = None
    try:
        template_git_repository = GitRepository(git.Repo(template_git_directory))
    except (git.InvalidGitRepositoryError, git.NoSuchPathError):
        click.echo(click.style(f'Git repository not found in "{template_git_directory}"', fg='yellow'))

    playbook_directory_path = Path(playbook_directory, playbook_name)
    readme_file_path = playbook_directory_path.joinpath('README.md')
    git_template = GitTemplate(template_git_repository)
    git_template.interpolate(readme_file_path, 'NDD_PLAYBOOK_TEMPLATE_')

    # TODO: replace the Bash script by a Python script
    import subprocess
    inventories_path = os.path.join(playbook_directory, playbook_name, 'inventories')
    link_commons_path = os.path.join(inventories_path, 'link-commons.sh')
    subprocess.call([link_commons_path])

    playbook_git_repository = GitRepository.initialize(playbook_directory_path)
    playbook_git_repository.git.add(all=True)
    playbook_git_repository.index.commit("Initial commit of the generated template")


if __name__ == "__main__":
    check_requirements()
    new_playbook()
